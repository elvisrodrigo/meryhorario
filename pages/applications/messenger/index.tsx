

import SidebarLayout from '@/layouts/SidebarLayout';
import FavoriteIcon from '@mui/icons-material/Favorite';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
/* import Mery from '@/content/Elvis/Principal/components/Mery'; */
import Frodry from '@/content/Elvis/Principal/components/Frodry';
import MuiAccordionSummary, {
  AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Grid } from '@mui/material';
import { Item } from '@/content/Elvis/Principal/components/ItemG';
const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === 'dark'
      ? 'rgba(255, 255, 255, .05)'
      : 'rgba(0, 0, 0, .03)',
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));




function ApplicationsMessenger() {

  return (
    <>
      <div style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)" }}>

        <div style={{ paddingTop: "0em" }}></div>
        <div>
         
          <Accordion style={{backgroundImage: 'url("/img/back66.jpg")',backgroundSize:"cover",backgroundRepeat:"no-repeat",color:"white"}} >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>RECORDATORIO</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          Por un beso de tu boca, dos abrazos te daría, tres suspiros que demuestran cuatro veces mi alegría y en la quinta sinfonía de mis seis sueños ardientes, siete veces 
          te diría, ocho veces que te quiero; nueve por ti vivo, diez por ti me elevo.........Si sumas todas las estrellas del cielo, todos los granitos de arena en los océanos,
           todas las rosas en el mundo y todas las sonrisas que haya habido en la historia, empezarás a tener una idea de cuánto te quiero......A tu lado aprendí que el amor es 
           algo más que una caricia, un beso o un deseo. Aprendí que el amor hay que entregarlo desinteresadamente, sin miedo, sin prejuicios, pero sí con exceso de cariño y ternura.
            Aprendí a confiar y decidí abrirte las puertas de mi corazón para que pudieras entrar y quedarte para siempre
          
                <p>-----------------------------------------------</p>
                <h3>TACTICA Y ESTRATEGIA </h3>
                <h4>Mi táctica es mirarte</h4>
                <h4>aprender como eres</h4>
                <h4>quererte como eres</h4>
                <h4>mi táctica es hablarte y escucharte</h4>
                <FavoriteIcon /><FavoriteIcon /><FavoriteIcon />

                <h4>construir con palabras un puente indestructible</h4>
                <h4>mi táctica es quedarme en tu recuerdo</h4>
                <h4>no sé cómo ni sé con qué pretexto</h4>
                <h4>pero quedarme en ti</h4>
                <h4>es todo lo que puedo desear</h4>
                <p>-----------------------------------------------</p>

                <h3>EL BUQUE DE LOS ENAMORADOS </h3>

                <h4>Era un buque en el mar,era el amor en medio de las olas inmensas,</h4>
                <h4>y era mi soledad de navegante y los peces oscuros de tus trenzas.</h4>
                <h4>Pensaba en ti, soñaba.Pensaba en ti, soñaba que iba contigo a perfumar los puertos</h4>
                <h4>y a sembrar anclas y constelaciones en las frentes dormidas de los muertos.</h4>
                <FavoriteIcon /><FavoriteIcon /><FavoriteIcon />
                <h4>Pero soñaba apenas, amor mío,y las aguas furiosas me sacaban del sueño,</h4>
                <h4>y a ti te separaban de mi costa, como una barca triste o como un leño.</h4>
                <h4>El buque, el buque entero, sin ti era un ataúd sobre las olas,un herido flotando tristemente</h4>
                <h4>sobre una muchedumbre de amapolas, me tambaleaba en medio de gaviotas,</h4>
                <FavoriteIcon /><FavoriteIcon /><FavoriteIcon />
                <h4>me inclinaba hacia ti salobremente, y las islas brillaban como lunas</h4>
                <h4>sobre toda la noche de mi frente, mar adentro no hay más que los recuerdos</h4>
                <h4>y sal sobre mi piel, sobre la vida, y el amor que pregunta por la sangre</h4>
                <h4>y le responde el labio de una herida, a veces era lunes,</h4>
                <FavoriteIcon /><FavoriteIcon /><FavoriteIcon />
                <h4>decían que era lunes mis hermanos, y te veía venir sobre las olas</h4>
                <h4>con toda la semana entre las manos, el tiempo era tu ausencia,</h4>
                <h4>el mar era la sombra de la tristeza mía,y el buque era un naufragio que se inclinaba</h4>
                <h4>y no se decidía, por la noche volaban las estrellas,</h4>
                <FavoriteIcon /><FavoriteIcon /><FavoriteIcon />
                <h4>como peces dorados, por el cielo, y yo pensaba que en la tierra firme</h4>
                <h4>tú también contemplabas este vuelo, el buque del amor, de los enamorados,</h4>
                <h4>todavía navega por mis venas, y levanta la espuma de mi sangre</h4>
                <h4>y la pescadería de mis penas, un rumor de marea que no cesa</h4>
                <FavoriteIcon /><FavoriteIcon /><FavoriteIcon />
                <h4>a pesar de los días y los pasos acomete la costa de mis besos</h4>
                <h4>y los acantilados de mis brazos, escucha el buque, esposa,</h4>
                <h4>acerca tus oídos a mi piel como flores,</h4>
                <h4>y escucha el buque, el buque, navegar por mis mares interiores.</h4>
                
          </Typography>
        </AccordionDetails>
      </Accordion>
      {/* <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>MONDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="14:00" e1="SIS-523 G1" f="REDES I 2do amb. 2" />
                <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-443 G1" d="INVESTIGACION OPERATIVA I 3er amb. 3" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="16:00" e1="SIS-518 G2" f="TALLER DE BASE DE DATOS 1er LAB. 7" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>TUESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="10:00" c1="SIS-517 G1" d="ANALISIS DE SISTEMAS II 2do amb. 2" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="16:00" e1="SIS-532 G1" f="SISTEMAS OPERATIVOS 1er LAB. 3" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>WESNESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-522 G1" d="ARQUITECTURA DE COMPUTADORAS 1er LAB. ROBOTICA" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="10:00" e1="SIS-517 G1" f="ANALISIS DE SISTEMAS II 2do amb. 2" />


          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>THURSDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="16:00" c1="SIS-532 G1" d="SISTEMAS OPERATIVOS 1er LAB. 3" />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>FRIDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="10:00" c1="SIS-523 G1" d="REDES I 1er LAB. 6" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="10:00" e1="SIS-443 G1" f="INVESTIGACION OPERATIVA I 3er amb. 7" />
                <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-522 G1" d="ARQUITECTURA DE COMPUTADORAS 1er LAB. ROBOTICA" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="16:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 7" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>SATURDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-518 G2" d="TALLER DE BASE DE DATOS 1er LAB. 6" />
          </Typography>
        </AccordionDetails>
      </Accordion> */}


        </div>

      </div>
      <div>
      <Grid style={{backgroundImage: 'url("/img/hh.jpeg")',backgroundSize:"cover",backgroundRepeat:"no-repeat"}} container spacing={0}  >
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          LUNES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
        10:00  SIS-645  2do AMB. 6
          <br />
          {'14:00  SIS-523  1er LAB. 6'}
          <br />
          {'14:00  SIS-634  2do LAB. 6'}
          <br />
          {'16:00 SIS-646 2do AMB. 2'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-645	SIMULACION DE SISTEMAS" d1="SIS-523	REDES I" d2="SIS-634	MODELOS ECONOMICOS" d3="SIS-646	INGENIERIA DE SOFTWARE"/>

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          MARTES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
        08:00  
          <br />
          {'10:00  SIS-624  1er LAB. 3'}
          <br />
          {'14:00  SIS-646 2do AMB. 2'}
          <br />
          {'16:00 '}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-624	CONFIGURACION DE SERVIDORES" d1="SIS-646	INGENIERIA DE SOFTWARE" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          MIERCOLES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00 SIS-633 2do AMB. 3
          <br />
          {'10:00  SIS-634 3ra AMB. 7'}
          <br />
          {'14:00  '}
          <br />
          {'16:00 SIS-624 1er LAB. 7'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-633	INGENIERIA DE SISTEMAS" d1="SIS-634	MODELOS ECONOMICOS" d2="SIS-624	CONFIGURACION DE SERVIDORES	"/>

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%"}}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          JUEVES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
            08:00 
            <br />
            {'10:00 '}
            <br />
            {'14:00 SIS-645 2do AMB. 6'}
            <br />
            {'14:00 '}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-645	SIMULACION DE SISTEMAS" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          VIERNES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00  SIS-633 2do AMB. 3
          <br />
          {'10:00  SIS-523  1er LAB. 6'}
          <br />
          {'14:00  '}
          <br />
          {'16:00 '}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-633	INGENIERIA DE SISTEMAS" d1="SIS-523	REDES I" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          SABADO
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00  
          <br />
          {'10:00  '}
          <br />
          {'14:00  '}
          <br />
          {'16:00 '}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="LASTIMOSAMENTE NO HAY CLASES Y ESO ES BUENO DIA DE DESCANSO" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
</Grid>
      </div>
    </>
  );
}

ApplicationsMessenger.getLayout = (page) => (
  <SidebarLayout>{page}</SidebarLayout>
);

export default ApplicationsMessenger;
