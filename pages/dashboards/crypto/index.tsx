import SidebarLayout from '@/layouts/SidebarLayout';
import { Mycomponentstwo } from '@/content/Elvis/Principal/components/Mycomponentstwo';
import * as React from 'react';
import Grid from "@mui/material/Grid";
import { Item } from '@/content/Elvis/Principal/components/ItemG';
import { Gallery } from '@/content/Elvis/Principal/components/Gallery';


function DashboardCrypto() {
  return (
    <>
      <section className="carusel">
        <div className="container-all">
          <input type="radio" id="1" name="image-slide" hidden />
          <input type="radio" id="2" name="image-slide" hidden />
          <input type="radio" id="3" name="image-slide" hidden />
          <div className="slide">
            <div className="item-slide">
              <img src="/img/d1.jpg" alt="" />
            </div>
            <div className="item-slide">
              <img src="/img/d3.jpg" alt="" />
            </div>
            <div className="item-slide">
              <img src="/img/d2.jpg" alt="" />
            </div>
          </div>
          <div className="pagination">
            <label className="pagination-item" htmlFor="1">
              <img src="/img/d1.jpg" alt="" />
            </label>
            <label className="pagination-item" htmlFor="2">
              <img src="/img/d3.jpg" alt="" />
            </label>
            <label className="pagination-item" htmlFor="3">
              <img src="/img/d2.jpg" alt="" />
            </label>

          </div>
        </div>
      </section>

      <div style={{ paddingTop: "5em" }}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={6} md={6}>
            <Item><Mycomponentstwo title="Lo que necesito de ti" paragraph="No sabes como necesito tu voz;
necesito tus miradas
aquellas palabras que siempre me llenaban,
necesito tu paz interior;
necesito la luz de tus labios
!!! Ya no puedo... seguir así !!!
...Ya... No puedo
mi mente no quiere pensar
no puede pensar nada más que en ti.
Necesito la flor de tus manos
aquella paciencia de todos tus actos
con aquella justicia que me inspiras
para lo que siempre fue mi espina
mi fuente de vida se ha secado
con la fuerza del olvido...
me estoy quemando;
aquello que necesito ya lo he encontrado
pero aun !!! Te sigo extrañando!!!" /></Item>
          </Grid>
          <Grid item xs={12} sm={6} md={6}>
            <Item style={{ margin: "1em", display: "flex", justifyContent: "center", alignItems: "center" }}><div data-aos="fade-left" data-aos-duration="2000" className="img-cont">
              <div className="img1"></div>
              <div className="img2"></div>
            </div></Item>
          </Grid>

        </Grid>
      </div>

      <div className="gallery" id="gallery">
        <div className="box-container">
          <Gallery imageUrl="/img/me1.png" name="Lunch" description="💗 Le he jurado a las estrellas que nunca te olvidare, por mas distancia que haya por siempre yo te querré." buttonValue="Read More " />
          <Gallery imageUrl="/img/m2.png" name="Freakfast" description="💗 Eres pura fantasía y realidad, eres cura y eres mal, remedio y enfermedad... Eres la culpable de alegrías que me colman esta vida y me traen hasta acá..." buttonValue="Read More " />
          <Gallery imageUrl="/img/m3.png" name="Dinner" description="💗 Todo lo que anhelo en esta vida el universo me lo ha concedido con tu presencia... Y es que debes saber que desde tu sonrisa hasta tu mirada hay un espacio infinito de amor y belleza que me salvan de la muerte y me llevan hasta el cielo... Te amo " buttonValue="Read More " />
          <Gallery imageUrl="/img/m4.png" name="Hotel" description="💗 Tengo un universo de palabras que atraviesan mi garganta dedicandote mi amor, tengo un universo de esperanza que te busca entre la gente para darte esto que soy, tengo una flor tierna y endulzada que te busca y no te encuentra, que quiere tener tu calor." buttonValue="Read More " />
          <Gallery imageUrl="/img/m5.png" name="Mobility" description="💗 Aquí estoy para ti, para que tu cabeza repose en mi pecho cuando algo salga mal, para que puedas reir y llorar sin tener miedo al que dirán, aquí estoy para que siempre sepas que tienes un lugar que te da el amor que te mereces por todo lo que siempre das." buttonValue="Read More " />

        </div>
      </div>
    </>
  );
}

DashboardCrypto.getLayout = (page) => <SidebarLayout>{page}</SidebarLayout>;

export default DashboardCrypto;
