import SidebarLayout from '@/layouts/SidebarLayout';
import * as React from 'react';
import Card from '@mui/material/Card';
/* import CardActions from '@mui/material/CardActions'; */
import CardContent from '@mui/material/CardContent';
import { Grid } from '@mui/material';
import { Item } from '@/content/Elvis/Principal/components/ItemG';
import Typography from '@mui/material/Typography';








function ManagementUserSettings() {
 
  return (
    <>
      <div style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)" }}>

      <Grid style={{backgroundImage: 'url("/img/back7.jpg")',backgroundSize:"cover",backgroundRepeat:"no-repeat"}} container spacing={0}  >
  
  <Grid   xs={3} sm={3} md={3}>
    <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>L</h1>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
   
    </Item >
  </Grid>
  <Grid   xs={3} sm={3} md={3}>
  <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>m</h1>
  <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          16:14 PM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-646
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          18:00 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={3} sm={3} md={3}>
  <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>j</h1>
  <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          08:00 AM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-523
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          10:00 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
    
  </Grid>
  <Grid   xs={3} sm={3} md={3}>
  <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>v</h1>
  <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          14:15 PM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-624
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          16:00 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
    
  </Grid>
  
</Grid>

      </div>
    </>
  );
}

ManagementUserSettings.getLayout = (page) => (
  <SidebarLayout>{page}</SidebarLayout>
);

export default ManagementUserSettings;
