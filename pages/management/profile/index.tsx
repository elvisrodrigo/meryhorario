import SidebarLayout from '@/layouts/SidebarLayout';
import * as React from 'react';
/* import { styled } from '@mui/material/styles'; */
/* import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import Mery from '@/content/Elvis/Principal/components/Mery'; */
/* import Frodry from '@/content/Elvis/Principal/components/Frodry'; */
/* import MuiAccordionSummary, {
  AccordionSummaryProps,
} from '@mui/material/AccordionSummary'; */
/* import MuiAccordionDetails from '@mui/material/AccordionDetails'; */
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
/* import CardActions from '@mui/material/CardActions'; */
import CardContent from '@mui/material/CardContent';
import { Grid } from '@mui/material';
import { Item } from '@/content/Elvis/Principal/components/ItemG';




/* const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));
 */


function ManagementUserProfile() {


  return (
    <>
      
      <div style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)" }}>



      <Grid style={{backgroundImage: 'url("/img/back7.jpg")',backgroundSize:"cover",backgroundRepeat:"no-repeat"}} container spacing={0}  >
  
  <Grid   xs={3} sm={3} md={3}>
    <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>L</h1>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          10:00 AM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-735
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          12:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={3} sm={3} md={3}>
  <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>m</h1>
  <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          18:30 PM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-735
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          20:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={3} sm={3} md={3}>
  <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>m</h1>
  <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          10:00 AM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-645
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          12:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          18:30 PM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-625
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          20:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={3} sm={3} md={3}>
  <h1 style={{color:"white" ,textAlign:"center",textTransform:"uppercase", fontWeight:"bold",fontSize:"4em"}}>v</h1>
  <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          10:00 AM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-624
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          12:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          10:00 AM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-625
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          12:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          10:00 AM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-645
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          12:15 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"90%" }}>
      <CardContent>
      <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingBottom="1em" color="text.secondary">
          14:15 PM
        </Typography>
        <Typography sx={{ fontSize: 14 }} alignItems="center" justifyContent="center" display="flex"  color="text.secondary" gutterBottom>
          AUXILIATURA
        </Typography>
        <Typography variant="h4"  alignItems="center" justifyContent="center" display="flex"  component="div">
          SIS-624
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" color="text.secondary">
          SEMESTRE 1/2024
        </Typography>
        <Typography sx={{ mb: 1.5 }} alignItems="center" justifyContent="center" display="flex" paddingTop="1em" color="text.secondary">
          16:00 PM
        </Typography>
      </CardContent>
      
    </Card>
      </div>
    </Item >
  </Grid>
  
</Grid>


       {/*  <div style={{ paddingTop: "5em" }}></div>
        <div>

          <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>MONDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>TUESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          
         
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="AUX SIS-544 G1" d="INVESTIGACION OPERATIVA II" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="14:00" e1="AUX SIS-523 G1" f="REDES I" />
                <Frodry data-aos="flip-right" data-aos-duration="2000" c="18:30" c1="AUX SIS-646 G1" d="INGENIERIA DE SOFTWARE" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>WESNESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          
          
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>THURSDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="AUX SIS-532 G1" d="SISTEMAS OPERATIVOS " />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="10:00" e1="AUX SIS-544 G1" f="INVESTIGACION OPERATIVA II" />
                <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:30" c1="AUX SIS-523 G1" d="REDES I" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>FRIDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="AUX SIS-532 G1" d="SISTEMAS OPERATIVOS " />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>SATURDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="AUX SIS-646 G1" d="INGENIERIA DE SOFTWARE " />
          </Typography>
        </AccordionDetails>
      </Accordion>
        </div>
 */}
      </div>
    </>
  );
}

ManagementUserProfile.getLayout = (page) => (
  <SidebarLayout>{page}</SidebarLayout>
);

export default ManagementUserProfile;
