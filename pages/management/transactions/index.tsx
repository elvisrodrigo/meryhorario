

import SidebarLayout from '@/layouts/SidebarLayout';
/* import ExpandMoreIcon from '@mui/icons-material/ExpandMore'; */
import * as React from 'react';
import { styled } from '@mui/material/styles';
/* import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp'; */
/* import MuiAccordion, { AccordionProps } from '@mui/material/Accordion'; */
/* import Mery from '@/content/Elvis/Principal/components/Mery'; */
import Frodry from '@/content/Elvis/Principal/components/Frodry';
/* import MuiAccordionSummary, {AccordionSummaryProps,} from '@mui/material/AccordionSummary'; */
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';

/* import Button from '@mui/material/Button'; */
import { Grid } from '@mui/material';
import { Item } from '@/content/Elvis/Principal/components/ItemG';
/* import { url } from 'inspector'; */
/* import { red } from '@mui/material/colors'; */
/* import { Margin } from '@mui/icons-material'; */
 
/* const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
})); */

/* const AccordionSummary = styled((props: AccordionSummaryProps) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === 'dark'
      ? 'rgba(255, 255, 255, .05)'
      : 'rgba(0, 0, 0, .03)',
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
  },
})); */

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));







function ApplicationsTransactions() {
  return (
    <>

      
      <Grid style={{backgroundImage: 'url("/img/hh.jpeg")',backgroundSize:"cover",backgroundRepeat:"no-repeat"}} container spacing={0}  >
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          LUNES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00  SIS-736  3ra AMB. 7
          <br />
          {'10:00  SIS-645  2do AMB. 6'}
          <br />
          {'14:00  SIS-735  1er LAB. 7'}
          <br />
          {'16:00'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-736 PREPARACION Y EVALUACION DE PROYECTOS" d1="SIS-645	SIMULACION DE SISTEMAS" d2="SIS-735	AUDITORIA DE SISTEMAS"/>

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          MARTES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
        08:00  SIS-625  1ra LAB. 6
          <br />
          {'10:00  SIS-624  1er LAB. 3'}
          <br />
          {'14:00  '}
          <br />
          {'16:00 SIS-747 2do AMB. 4'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-625	TALLER DE REDES" d1="SIS-624	CONFIGURACION DE SERVIDORES" d2="SIS-747	METODOLOGIA DE LA INVESTIGACION	"/>

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          MIERCOLES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00  
          <br />
          {'10:00  SIS-736 2do AMB. 7'}
          <br />
          {'14:00 OPT-005 1er LAB. Rot. '}
          <br />
          {'16:00 SIS-624 1er LAB. 7'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-736	PREPARACION Y EVALUACION DE PROYECTOS" d1="	OPT-005	ROBOTICA" d2="SIS-624	CONFIGURACION DE SERVIDORES	"/>

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          JUEVES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
            10:00 SIS-625 1ra LAB. 2
            <br />
            {'10:00  SIS-735 2do AMB. 7'}
            <br />
            {'14:00 SIS-645 2do AMB. 6'}
            <br />
            {'14:00 SIS-747 2do AMB. 5'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="SIS-625	TALLER DE REDES" d1="SIS-735	AUDITORIA DE SISTEMAS" d2="SIS-645	SIMULACION DE SISTEMAS" d3="SIS747	METODOLOGIA DE LA INVESTIGACION	" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          VIERNES
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00  
          <br />
          {'10:00  '}
          <br />
          {'14:00  '}
          <br />
          {'16:00 OPT-005 1er LAB. Rob.'}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="OPT005	ROBOTICA BASICA LABORATORIO I" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
  <Grid   xs={6} sm={4} md={4}>
    <Item  marginBottom={0.5} marginLeft={0.5} marginTop={0.5} marginRight={0.5}>
    <div  >
      <Card style={{background: "rgba(255,255,255,0.65)",backdropFilter:'blur(2px)',WebkitBackdropFilter:'blur(1px)'}} sx={{ minWidth:"100%" }}>
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          SEMESTRE 1/2024
        </Typography>
        <Typography variant="h2" component="div">
          SABADO
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary">
          Ingenieria de Sistemas
        </Typography>
        <Typography variant="body2">
          08:00  
          <br />
          {'10:00  '}
          <br />
          {'14:00  '}
          <br />
          {'16:00 '}
        </Typography>
      </CardContent>
      <CardActions>
      <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="" c1="ver mas" d="LASTIMOSAMENTE NO HAY CLASES Y ESO ES BUENO DIA DE DESCANSO" />

          </Typography>
        </AccordionDetails>
      </CardActions>
    </Card>
      </div>
    </Item >
  </Grid>
</Grid>


      {/* <div style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)" }}>

        <div style={{ paddingTop: "5em" }}></div>
        <div>
        <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>REMINDER</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          MY HEART RACES AS IF IT WAS MY OWN STORY, I AM EXCITED AND FILLED WITH HOPE 
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>MONDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-544 G1" d="INVESTIGACION OPERATIVA II 2do amb. 6" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="14:00" e1="SIS-523 G1" f="REDES I 2do amb. 2" />
                <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-634 G1" d="MODELOS ECONOMICOS 2do amb .6" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="16:00" e1="SIS-646 G1" f="INGENIERIA DE SOFTWARE 2do amb .2" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>TUESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-646 G1" d="INGENIERIA DE SOFTWARE 2do amb .2" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="16:00" e1="SIS-532 G1" f="SISTEMAS OPERATIVOS 1er LAB 3" />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>WESNESDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-633 G1" d="INGENIERIA DE SISTEMAS 2do amb. 3" />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>THURSDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="16:00" c1="SIS-532 G1" d="SISTEMAS OPERATIVOS 1er LAB 3" />
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>FRIDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
          <Frodry data-aos="flip-right" data-aos-duration="2000" c="08:00" c1="SIS-633 G1" d="INGENIERIA DE SISTEMAS 2do amb. 4" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="10:00" e1="SIS-523 G1" f="REDES I 1er LAB. 6" />
                <Frodry data-aos="flip-right" data-aos-duration="2000" c="14:00" c1="SIS-634 G1" d="MODELOS ECONOMICOS 2do amb .6" />
                <Mery data-aos="flip-right" data-aos-duration="2000" e="16:00" e1="SIS-544 G1" f="INVESTIGACION OPERATIVA II 2do amb. 7" />

          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ "background": "linear-gradient(to right, #4b134f, #c94b4b)", color: "white" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography>SATURDAY</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
         
          </Typography>
        </AccordionDetails>
      </Accordion>
       
        </div>

      </div> */}
    </>
  );
}

ApplicationsTransactions.getLayout = (page) => (
  <SidebarLayout>{page}</SidebarLayout>
);

export default ApplicationsTransactions;
